# bed2faldo

Convert [BED](https://genome.ucsc.edu/FAQ/FAQformat.html#format1) data into [FALDO](https://github.com/OBF/FALDO)  (tabulated or RDF)

# Requirements

- [bed2pandas](https://gitlab.com/odameron/bed2pandas)


# Todo

- [ ] generate tabulated files
- [ ] generate RDF files
    - [ ] named graph
- [ ] generate metadata
    - [ ] VoID

